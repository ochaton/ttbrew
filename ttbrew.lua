#!/usr/bin/env tarantool
local log = require 'log'
local fio = require 'fio'
local fun = require 'fun'
local http = require 'http.client'
local json = require 'json'

local version=[[dev]]

local ttbrew = require 'argparse'()
	:name("ttbrew "..version)
	:add_help_command()
	:add_complete()
	:help_vertical_space(1)
	:help_description_margin(60)
	:command_target "command"
	:description "Use your own Tarantool"

local install = ttbrew:command "install"
	:summary "Installs Tarantool from https://github.com/tarantool/tarantool"

install:flag "-f" "--force"
	:description "force installation"
	:target "install_force"

install:flag "-n" "--notest"
	:description "Skip testing"
	:target "install_notest"

install:option "--destdir"
	:description "Install tt as 'make install -DCMAKE_INSTALL_PREFIX=$destdir'"
	:target "install_destdir"

install:argument "version"
	:args "1"
	:target "install_version"
	:description "Version of Tarantool (tag, semver or commit)"

ttbrew:command "list"
	:summary "Lists all installed tarantools"

ttbrew:command "switch"
	:summary "Switches ~/bin/tarantool to specified version"
	:argument "version"
	:args "1"
	:target "switch_version"
	:description "Version of installed Tarantool"

ttbrew:command "search"
	:summary "Searches through all Tarantool releases"

ttbrew:command "info"
	:summary "Gets info about specified release"
	:argument "version"
	:args "1"
	:target "info_version"
	:description "Release version of Tarantool"

local args = assert(ttbrew:parse())

local function parse_version(v)
	local maj, min, pat = v:match("^([0-9]+)%.([0-9]+)%.([0-9]+)$")
	if maj then return v end

	maj, min = v:match "^([0-9+)%.([0-9]+)$"
	if maj then return v..".0" end

	error("Version cannot be parsed")
end

local function installed_tarantools(root)
	local r = fio.listdir(root)
	table.sort(r)
	return r
end

local function get_releases()
	local res = http.get('https://api.github.com/repos/tarantool/tarantool/releases', { headers = {
		['User-Agent'] = 'tarantool/'.._TARANTOOL,
	} })
	if res.status ~= 200 then
		log.error("gh api failed: %s:%s", res.status, res.body)
		os.exit(1)
	end

	local releases = json.decode(res.body)
	table.sort(releases, function(a, b) return a.published_at > b.published_at end)

	return releases
end

local function get_release(v)
	for _, r in ipairs(get_releases()) do
		if r.tag_name == v then
			return r
		end
	end
end

os.exit(
	({
		install = function()
			log.info("Installing Tarantool %s", args.install_version)
			local tempdir = (fio.tempdir())

			log.info("Temporary directory created: %s", tempdir)
			assert(fio.chdir(tempdir))

			log.info("chdir %s succesfull", fio.cwd())

			local version = parse_version(args.install_version)
			log.info("Installing version: %s", version)

			local r = get_release(version)
			if not r then
				log.error("Version %s was never released", version)
				os.exit(1)
			end

			os.execute(
				[[ git clone --recurse-submodules --jobs 8 --branch ]] .. version
				..[[ https://github.com/tarantool/tarantool.git ]]
				..[[ && cd tarantool && git submodule update --init --recursive --force ]]
				..[[ && cmake . -DCMAKE_C_FLAGS="-Wno-sizeof-array-div -Wno-missing-exception-spec" ]]
				..[[ -DCMAKE_CXX_FLAGS="-Wno-missing-exception-spec" -DCMAKE_BUILD_TYPE=RelWithDebInfo ]]
				..[[ -DCMAKE_INSTALL_PREFIX="$HOME/tarantool/$(git describe --tags --abbrev=0)" ]]
				..[[ && make -j install ]]
			)
		end,

		list = function()
			local home = os.getenv "HOME"
			local root = home.."/tarantool/"
			local dirs = installed_tarantools(root)

			local installed = fio.readlink(home.."/bin/tarantool")

			for _, dir in ipairs(dirs) do
				local fullpath = fio.abspath(root .. '/' .. dir)
				if installed and installed:sub(1, #fullpath) == fullpath then
					log.info("%-10s => %s", "* "..dir, fullpath)
				else
					log.info("%-10s => %s", dir, fullpath)
				end
			end
		end,

		search = function()
			local home = os.getenv "HOME"
			local root = home.."/tarantool/"
			local dirs = installed_tarantools(root)

			local kv = fun.iter(dirs):zip(fun.ones()):tomap()
			for _, release in ipairs(get_releases()) do
				if kv[release.tag_name] then
					log.info("I %-10s\t%-40s\t%20s", release.tag_name, release.name, release.published_at)
				else
					log.info("* %-10s\t%-40s\t%20s", release.tag_name, release.name, release.published_at)
				end
			end
		end,

		info = function()
			local release = get_release(parse_version(args.info_version))
			log.info(release.body)
		end,

		switch = function()
			local home = os.getenv "HOME"
			local root = home.."/tarantool/"

			local version = parse_version(args.switch_version)
			local dirs = installed_tarantools(root)

			local d do
				for _, v in ipairs(dirs) do
					if v == version then d = v break end
				end
				if not d then
					log.error("Tarantool %s is not installed", version)
					os.exit(1)
				end
			end

			if fio.readlink(home.."/bin/tarantool") then
				log.verbose("Removing symlink")
				assert(fio.unlink(home.."/bin/tarantool"))
			end

			assert(fio.symlink(root..version.."/bin/tarantool", home.."/bin/tarantool"))

			print(([[export PATH="%s:$PATH"]]):format(home.."/bin"))
		end,
	})[args.command]()
)
