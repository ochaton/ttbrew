rockspec_format = "3.0"
package = "ttbrew"
version = "scm-1"
source = {
   url = "git+https://gitlab.com/ochaton/ttbrew.git",
}
description = {
   homepage = "https://gitlab.com/ochaton/ttbrew",
   license = "BSD"
}
dependencies = {
   "argparse",
}
build = {
   type = "builtin",
   modules = {},
   install = {
      bin = {
         ttbrew = "ttbrew.lua",
      },
   }
}
